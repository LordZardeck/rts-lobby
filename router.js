console.log('Starting server...');
var server = require('http').createServer(function(req, res){
	res.writeHead(301, {'Location': 'http://play.redemptionconnect.com'});
	res.end();
});
server.listen(8081);

var nowjs = require("now"), http = require('http'), util = require('util');
var everyone = nowjs.initialize(server);

var net = require('net'), clients = [];

function command(cmd, data, socket)
{
	var cmdBuffer = null, buffSize = 2, pointer = 0, cmdLen = Buffer.byteLength(cmd, 'ascii'), dataLen = Buffer.byteLength(data, 'ascii');
	
	buffSize += cmdLen + dataLen;
	cmdBuffer = new Buffer(buffSize);
	
	//Write command header
	pointer = cmdBuffer.write('b8', 0, 1, 'hex');
	//Add command string
	pointer += cmdBuffer.write(cmd, pointer, cmdLen, 'ascii');
	//Write command delimiter
	pointer += cmdBuffer.write('bb', pointer, 1, 'hex');
	//Write command data
	cmdBuffer.write(data, pointer, dataLen, 'ascii');
	
	try
	{
		socket.write(cmdBuffer);
	}
	catch(e)
	{
		util.log('Socket Write Error');
		util.inspect(e);
	}
}
var server = net.createServer(function(clientSocket){
	if(!clients[clientSocket.remoteAddress])
	{ 
		clientSocket.msgBuffer = new Array();
		clientSocket.setKeepAlive(true);
		clients[clientSocket.remoteAddress] = clientSocket;
	}
	everyone.getUsers(function(users){
		for(var i = 0; i < users.length; i++)
		{
			nowjs.getClient(users[i], function(){
				if(this.socket.handshake.address.address == clientSocket.remoteAddress)
				{
					var player = this;
					clientSocket.player = this;
					nowjs.getGroup(this.now.serverRoom).now.receiveMessage('ROOM', this.now.name + ' is playing RTS');
					if(player.now.serverRoom == 'Global')
					{
						command("noticem", "You cannot join a game in the global room", clientSocket);
						delete clients[clientSocket.remoteAddress];
						clientSocket.end();
						return;
					}
					nowjs.getGroup(this.now.serverRoom).getUsers(function(opps){
						for(var i = 0; i < opps.length; i++)
						{
							nowjs.getClient(opps[i], function(){
								if(this.socket.handshake.address.address != clientSocket.remoteAddress)
								{
									clientSocket.opponent = this;
									if(clients[this.socket.handshake.address.address] && !clients[this.socket.handshake.address.address].opponent)
										clients[this.socket.handshake.address.address].opponent = player;
									if(clients[clientSocket.opponent.socket.handshake.address.address] && clients[clientSocket.opponent.socket.handshake.address.address].msgBuffer.length > 0)
									{
										for(var a = 0; a < clients[clientSocket.opponent.socket.handshake.address.address].msgBuffer.length; a++)
										{
											try
											{
												clientSocket.write(clients[clientSocket.opponent.socket.handshake.address.address].msgBuffer[a]);
											}
											catch (e)
											{
												util.log('Socket Write Error');
												util.inspect(e);
											}
										}
										clients[clientSocket.opponent.socket.handshake.address.address].msgBuffer = new Array();
									}
								}
							});
						}
					});
				}
			});
		}
	});
	if(!clientSocket.opponent || !clients[clientSocket.opponent.socket.handshake.address.address])
	{
		command("noticem", "Opponnent not connected yet. Game will start, but please wait for opponent to connect to game.", clientSocket);
	}
	clientSocket.on('data', function(data){
		try
		{
			if(clientSocket.opponent && clients[clientSocket.opponent.socket.handshake.address.address])
				clients[clientSocket.opponent.socket.handshake.address.address].write(data);
			else
			{
				clientSocket.msgBuffer.push(data);
			}
		}
		catch(e){console.log(e);}
	});
	clientSocket.on('close', function(had_error){
		var msg = 'Client ' + clientSocket.remoteAddress + ' closed'; if(had_error) util.log(msg + ' unexpectedly'); else util.log(msg);
		
		for(address in clients)
		{
			if(clients[address] == clientSocket)
			{
				delete clients[address];
				for(a in clients)
				{
					if(clients[a].opponent.socket.handshake.address.address == address)
					{
						command("noticem", "Opponent has disconnected", clients[a]);
						clients[a].end();
					}
				}
			}
		}
	});
}).listen(1337);

address = server.address();
console.log("Opened routing server on %j", address);
console.log('Server ready.');

nowjs.on('connect', function(){
	var self = this;
	nowjs.getGroups(function(groups){
		for(var i = 0; i < groups.length; i++)
		{
			if(groups[i] != 'everyone')
				nowjs.getGroup(groups[i]).count(function(c){everyone.now.roomCreated(groups[i], c);});
		}
	});
	var globalGroup = nowjs.getGroup('Global');
	this.now.receiveMessage('SYSTEM', 'Welcome to Redemption Connect!');
});

nowjs.on('newgroup', function(g){
	g.on('join', function(){
		var self = this;
		g.now.receiveMessage('SYSTEM', this.now.name + ' has entered the room.');
		g.count(function(c){
			//if(c == 1 && this.now.serverRoom != 'Global') self.now.receiveMessage('Invite players to join you by giving them this url: <a href="http://play.redemptionconnect.com/?joinRoom=' + this.now.serverRoom + '">http://play.redemptionconnect.com/?joinRoom=' + this.now.serverRoom + '</a>');
			updateUserCount(g, c);
			syncRooms(g);
		});
	});
	g.on('leave', function(){
		var self = this;
		g.now.receiveMessage('SYSTEM', self.now.name + ' has left the room.');
		g.hasClient(this.user.clientId, function(bool){
			var cnt;
			if(bool)
				cnt = 1;
			else
				cnt = 0;
			g.count(function(c){
				if(c == cnt && g.groupName != 'everyone' && g.groupName != 'Global') 
					setTimeout(function(){nowjs.removeGroup(g.groupName);}, 500);
				
				if(cnt == 1)
					c--;
				updateUserCount(g, c);
			});
		});
	});
	g.count(function(c){everyone.now.roomCreated(g.groupName, c);});
});

nowjs.on('removegroup', function(g){
	if(g != 'everyone' && g != 'Global')
		everyone.now.roomDestroyed(g);
});

everyone.now.distributeMessage = function(message){
	var group = nowjs.getGroup(this.now.serverRoom);
    group.now.receiveMessage(this.now.name, message);
};

everyone.now.changeRoom = function(newRoom){
    var oldRoom = this.now.serverRoom, newGroup = nowjs.getGroup(newRoom), canJoin = false, self = this;
	if(newRoom == 'Global' || newRoom == 'everyone')
		canJoin = true;
	else
		newGroup.count(function(c){if(c < 2) canJoin = true;});
	
	if(canJoin === false)
	{
		this.now.alert('Cannot join ' + newRoom + '. Room full');
		return;
	}

    //if old room is not null; then leave the old room
    if(oldRoom)
        nowjs.getGroup(oldRoom).removeUser(this.user.clientId);

    // join the new room
    newGroup.addUser(this.user.clientId);

    // update the client's serverRoom variable
	this.now.serverRoom = newRoom;
	this.now.roomChanged(newRoom);
};

everyone.now.syncGroup = function(name)
{
	syncRooms(nowjs.getGroup(name));
}

everyone.now.lfg = function()
{
	var options = {
	  host: 'www.cactusgamedesign.com',
	  port: 80,
	  path: '/message_boards/index.php?action=portal;sa=shoutbox;xml',
	  method: 'POST',
	  headers: 
	  {
		'Content-Type': 'application/x-www-form-urlencoded', 
		'Cookie': 'SMFCookie10=a%3A4%3A%7Bi%3A0%3Bs%3A4%3A%221896%22%3Bi%3A1%3Bs%3A40%3A%22004bc523a6072b6c8587d5017d769b0a37fcb499%22%3Bi%3A2%3Bi%3A1516842114%3Bi%3A3%3Bi%3A0%3B%7D; PHPSESSID=c2cd5fcc5c37928000d166fb56d9d432', 
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'
	}
	}, reqString = '[url=http://play.redemptionconnect.com/?joinRoom=' + this.now.serverRoom + ']' + this.now.name + ' is lfg[/url]';

	var req = http.request(options, function(res) {
	  console.log('LFG STATUS: ' + res.statusCode);
	});

	req.on('error', function(e) {
	  console.log('problem with request: ' + e.message);
	});

	// write data to request body
	req.write('shoutbox_id=1&shout=' + encodeURI(reqString) + '&e45bb0ecfad=91141932c673fb24a06733729c93dabf');
	req.end();
}
function syncRooms(g)
{
	var usernames = new Array(), c;
	g.getUsers(function(users){
		c = users.length;
		if(c == 0)
		{
			everyone.now.groupSynced(g.groupName, usernames);
		}
		for(var i = 0; i < users.length; i++)
		{
			nowjs.getClient(users[i], function(){
				usernames.push(this.now.name);
				if(usernames.length == c)
				{
					everyone.now.groupSynced(g.groupName, usernames);
				}
			});
		}
	});
}
function updateUserCount(g, c){ everyone.now.countUpdate(g.groupName, c); }